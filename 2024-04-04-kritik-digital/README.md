# Den Digitale Selvhjælpscafé 

* Intro

* Den kooperative model, definitioner og erfaringer fra teknologi/internet-basserede kooperativer (det her er lidt et læringspunkt for mig, da jeg er blevet opmærksom på, at en enkelt god definition på forskellige tech-relaterede kooperative modeller)

* Hvordan data.coop er struktureret

  * Badges og services
  * Foreningen
  * Tillidsmodellen

* Føderation - hvad er det, og hvordan minder det om email?

* Udfordringer i kooperative modeller ud fra vores erfaringer

  * Penge
  * Deltagelse
  * Moderation

* Et bud på, hvordan fremtiden burde se ud, hvor vi gerne vil hen med data.coop 


## Fra Kritik Digital


> Konceptet kondenseret i bullets:
>

> - Inddrage os selv som tvivlere
> - Forsøge at nedbryde videnshierarkier - have et åbent forum
> - Inddrage gæster på en mere aktiv måde: hvordan giver vi folk lyst til at sige noget
> - Vidensasymmetrien er et fælles anliggende: vi vil ikke sidde og researche alene, men gribe anledningen til at åbne vores tvivl op og lade processen komme flere til gavn
>

## Arrangementet

Den digitale selvhjælpscafé er tilbage!

Med den digitale selvhjælpscafé skaber Kritik Digital et rum, hvor du kan lufte dine bekymringer og udfordringer med den digitale verden.

Gennem vores caféer ønsker vi at inspirere andre aktivister og organisationer til at reflektere over deres eget digitale liv - både individuelt og kollektivt. Vi vil gerne skabe et rum til at blive klogere på spørgsmål om, hvor vores kommunikation bliver hostet, hvorfor det overhovedet er vigtigt at passe på sine data, og hvad det vil kræve af os at skifte fra de store og altomsluttende platforme som Google, Meta og Discord.

Aftenens program:

Oplæg fra data.coop – Aftenens gæst, data.coop, fortæller om foreningens arbejde og gør os klogere på alternative løsninger til at sikre os selv og vores data, når vi færdes i den digitale verden.

Pause – Efter oplægget vil der være mulighed for at summe og reflektere over de præsenterede emner og eventuelt formulere sig nogle spørgsmål til data.coop.

Q&A Session - Stil dine spørgsmål og få svar fra data.coop. Kritik Digital vil sparke gang i debatten, og herefter tager vi os tid til at diskutere og adressere tvivl, udfordringer og problematikker.

Til caféen vil der være både være drikkevarer og snacks i baren.

Arrangementet er åbent for alle, så tag gerne din venner under armen. Vi glæder os til at se dig!

(Arrangementet er finansieret af fondsmidler fra FRi-Puljen)

Sted
Ungdommens Demokratihus
Slagtehusgade 10a
1715 København 
